# Ntuple-Production

This repository is a software for producing ntuples using AnalysisBase (AB). Input should be a TOPQ1 derivation. Output contains information about photons, leptons, jets in the ntuple.

## Review the setup script and config files
Check the release version of AB in the setup.sh script. 

Replace the `RUCIO_ACCOUNT` variable value with your CERN username

You can find the latest release of the AB here. https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisBaseReleaseNotes21_2

Review and change the content of input config files for `top-xaod` listed here: source/TtGamma/share/config*.txt 

## Build the software
Instructions for building
#### First time
```
git clone https://:@gitlab.cern.ch:8443/SM-tqGamma/ntuple-production.git
cd ntuple-production/
mkdir build/ run/
source ../setup.sh
cd ../build
cmake ../source/
make
source x86*/setup.sh
```
#### If you want to build with a new AB release
If you want build the software with new AB release, either delete the previous build/ folder or create a new build folder with a different name.
```
cd ntuple-production/
source ../setup.sh/
rm -rf build/
mkdir build/
cmake ../source/
make
source x86*/setup.sh
```
#### Always in a new session
```
cd ntuple-production
source ../setup.sh
source x86*/setup.sh
```
## Test the software locally
Always test the software locally before submitting jobs to grid. For this, you need to download a TOPQ1 derivation file to your computer. 

Downloading a test file:
``` 
rucio download mc16_13TeV.412147.aMcAtNloPy8EG_A14NNPDF23_tqgammaSM_tchan_4fl_NLO.deriv.DAOD_TOPQ1.e7782_a875_r9364_p4031 --nrandom 1
```
```
cd ../run/
cp ../source/TtGamma/share/config*.txt .
top-xaod config_mc16d.txt in.txt
```
*in.txt* should contain the path to the input file. (e.g. /data/harish/TOPQ1/data18_13TeV/DAOD_TOPQ1.14279071._000069.pool.root.1)

## Submit jobs to grid
After building the software, ntuple-production jobs can be submitted to grid using 01SubmitToGrid.py. 

Change the values for `config.gridUsername`, `config.suffix`, list of samples (check MC16a/d/e_TOPQ1.py and Data_rel21.py) inside this script.

```
cd ../run/
cp ../source/TtGamma/share/config*.txt .
cp ../source/TtGamma/scripts/*.py .
python 01SubmitToGrid.py mc16a,mc16d,mc16e
```

