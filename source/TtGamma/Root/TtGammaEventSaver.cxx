#include "TtGamma/TtGammaEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "TopEvent/EventTools.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "PATInterfaces/SystematicSet.h"
#include "TopConfiguration/TopConfig.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
#include "xAODPrimitives/IsolationHelpers.h"
#include "TopCPTools/TopIsolationCPTools.h"
//#include "FakeBkgTools/AsymptMatrixTool.h"
#include "PathResolver/PathResolver.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventAuxInfo.h"
#include "xAODBase/IParticle.h"
#include "xAODEgamma/Electron.h"
#include "xAODMuon/Muon.h"
//#include <TopFakes/FakesWeights.h>
#include "TLorentzVector.h"
//#include "TopFakes/TopFakesxAODUtils.h"
#include "IsolationCorrections/IIsolationCorrectionTool.h"
#include "IsolationCorrections/IsolationCorrection.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "IsolationSelection/IsolationLowPtPLVTool.h"
#include "IsolationCorrections/IsolationCorrectionTool.h"


//using namespace FakeBkgTools;
/*
 * Custom EventSaver for TtGamma
 * May 2016
 * Julien Caudron
 *
 * updated by Gregor Gessner on 2018-11-22
 * updated by Bjoern Wendland on 2019-11-19
 * Harish Potti: Add isoFCT & isoFCL to the output
 * To Do: Top quark truth information
*/


namespace top{

    TtGammaEventSaver::TtGammaEventSaver(): m_vgamma_tool{"VGammaORTool/VGammaORTool"}
    {


		}

    void TtGammaEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
    {
        EventSaverFlatNtuple::initialize(config, file, extraBranches);

        m_configMine = config;

        //---------- initialize two instances of Photon Selector in order to make custom ID selection (needed for hadron fake study)
        // current recommendations for photons are listed here: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaRecommendationsR21
        // config files stored here: http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/

        m_photonTightIsEMSelector = new AsgPhotonIsEMSelector ("PhotonTightIsEMSelector");
        top::check(m_photonTightIsEMSelector->setProperty("isEMMask", egammaPID::PhotonTight), "photonTightIsEMSelecor failed");
        top::check(m_photonTightIsEMSelector->setProperty("ConfigFile", "ElectronPhotonSelectorTools/offline/20180825/PhotonIsEMTightSelectorCutDefs.conf"), "photonTightIsEMSelecor failed");
        top::check(m_photonTightIsEMSelector->initialize(), "photonTightIsEMSelecor failed");

        m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLooseIsEMSelector" );
        top::check(m_photonLooseIsEMSelector->setProperty("isEMMask", egammaPID::PhotonLoose), "photonLooseIsEMSelecor failed");
        top::check(m_photonLooseIsEMSelector->setProperty("ConfigFile", "ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf"), "photonLooseIsEMSelecor failed");
        top::check(m_photonLooseIsEMSelector->initialize(), "photonLooseIsEMSelecor failed");

        top::check(m_vgamma_tool.setProperty("dR_lepton_photon_cut", 0.2), "Could not set VGammaORTool lepton-photon dR cut");
		    top::check(m_vgamma_tool.setProperty("photon_pT_cuts", std::vector<float>{15e3}), "Could not set VGammaORTool photon pT cut");
		    top::check(m_vgamma_tool.initialize(), "Could not initialize VGammaORTool");



		// Top Fakes




        //---------- scale factor for MC
        //---------- check here for infos: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PhotonEfficiencyRun2

        if (m_configMine->isMC()) {

            int dataType; //0 for Data, 1 for MC full-sim, 3 for MC fast-sim
            if(m_configMine->isAFII()) { dataType = 3; }
            else { dataType = 1; }

            m_photonEfficiencyCorrectionTool = new AsgPhotonEfficiencyCorrectionTool("PhotonEfficiencyCorrectionToolEventSaver");
            top::check(m_photonEfficiencyCorrectionTool->setProperty("MapFilePath", "PhotonEfficiencyCorrection/2015_2017/rel21.2/Summer2018_Rec_v1/map4.txt"), "photonEfficiencyCorrectionTool failed 1");
            top::check(m_photonEfficiencyCorrectionTool->setProperty("ForceDataType", dataType), "photonEfficiencyCorrectionTool failed 2");
            top::check(m_photonEfficiencyCorrectionTool->setProperty("IsoKey", "Tight"), "photonEfficiencyCorrectionTool failed 3");
            top::check(m_photonEfficiencyCorrectionTool->initialize(), "photonEfficiencyCorrectionTool failed 4");
        };

        //--------- Setting up the isolation correction tool to apply most recent corrections to calorimeter isolation variables (topoetconeXX)

        /*m_isoCorrTool = new CP::IsolationCorrectionTool("isoCorrTool");
        top::check( asg::setProperty( m_isoCorrTool,  "Apply_SC_leakcorr", true), "Applying leakage correction failed");
        top::check( asg::setProperty( m_isoCorrTool,  "Apply_etaEDParPU_correction", true), "Applying leakage correction failed");

        if(m_configMine->isMC())   top::check( asg::setProperty( m_isoCorrTool,  "Apply_etaEDPar_mc_correction", true), "Applying leakage correction failed");
        else  top::check( asg::setProperty( m_isoCorrTool,  "Apply_etaEDPar_mc_correction", false), "Applying leakage correction failed");

        top::check( asg::setProperty( m_isoCorrTool,  "Apply_ddshifts", false), "Applying leakage correction failed");
        top::check( m_isoCorrTool->initialize(),  "Could not initialize isolation correction tool");*/



        //---------- initialize the new variables
        for (auto systematicTree : treeManagers()) {

            //---- isolation
            systematicTree->makeOutputVariable(m_ph_topoetcone20, "ph_topoetcone20");
            systematicTree->makeOutputVariable(m_ph_topoetcone30, "ph_topoetcone30");
            systematicTree->makeOutputVariable(m_ph_topoetcone40, "ph_topoetcone40");
            //---- new corrections to calorimeter isolation of photons

            /*systematicTree->makeOutputVariable(m_ph_topoetcone20_IsoCorrTool, "ph_topoetcone20_IsoCorrTool");
            systematicTree->makeOutputVariable(m_ph_topoetcone40_IsoCorrTool, "ph_topoetcone40_IsoCorrTool");*/



            systematicTree->makeOutputVariable(m_ph_ptcone20,     "ph_ptcone20");
            systematicTree->makeOutputVariable(m_ph_ptcone30,     "ph_ptcone30");
            systematicTree->makeOutputVariable(m_ph_ptcone40,     "ph_ptcone40");
	    systematicTree->makeOutputVariable(m_ph_isoFCTCO,     "ph_isoFCTCO");
            systematicTree->makeOutputVariable(m_ph_isoFCT,       "ph_isoFCT");
            systematicTree->makeOutputVariable(m_ph_isoFCL,       "ph_isoFCL");

            //---- new track isolation variables
            /*systematicTree->makeOutputVariable(m_ph_ptcone20_TightTTVA_pt500,     "ph_ptcone20_TightTTVA_pt500");
            systematicTree->makeOutputVariable(m_ph_ptcone20_TightTTVA_pt1000,     "ph_ptcone20_TightTTVA_pt1000");
            systematicTree->makeOutputVariable(m_ph_ptvarcone30_TightTTVA_pt500,     "ph_ptvarcone30_TightTTVA_pt500");
            systematicTree->makeOutputVariable(m_ph_ptvarcone30_TightTTVA_pt1000,     "ph_ptvarcone30_TightTTVA_pt1000");*/
            //---- identification
            systematicTree->makeOutputVariable(m_ph_isTight,      "ph_isTight");
            systematicTree->makeOutputVariable(m_ph_isLoose,      "ph_isLoose");
            systematicTree->makeOutputVariable(m_ph_isTight_daod, "ph_isTight_daod");
            systematicTree->makeOutputVariable(m_ph_isHadronFake, "ph_isHFake");

            systematicTree->makeOutputVariable( m_ph_rhad1,  "ph_rhad1");
            systematicTree->makeOutputVariable( m_ph_rhad,   "ph_rhad");
            systematicTree->makeOutputVariable( m_ph_reta,   "ph_reta");
            systematicTree->makeOutputVariable( m_ph_weta2,  "ph_weta2");
            systematicTree->makeOutputVariable( m_ph_rphi,   "ph_rphi");
            systematicTree->makeOutputVariable( m_ph_ws3,    "ph_ws3");
            systematicTree->makeOutputVariable( m_ph_wstot,  "ph_wstot");
            systematicTree->makeOutputVariable( m_ph_fside,  "ph_fracm");
            systematicTree->makeOutputVariable( m_ph_deltaE, "ph_deltaE");
            systematicTree->makeOutputVariable( m_ph_eratio, "ph_eratio");
            systematicTree->makeOutputVariable( m_ph_emaxs1, "ph_emaxs1");
            systematicTree->makeOutputVariable( m_ph_f1,     "ph_f1");
            systematicTree->makeOutputVariable( m_ph_e277,   "ph_e277");

            //---- reconstruction
            systematicTree->makeOutputVariable(m_ph_OQ,             "ph_OQ");
            systematicTree->makeOutputVariable(m_ph_author,         "ph_author");
            systematicTree->makeOutputVariable(m_ph_conversionType, "ph_conversionType");
            systematicTree->makeOutputVariable(m_ph_caloEta,        "ph_caloEta");
            systematicTree->makeOutputVariable(m_ph_isEM_Tight,     "ph_isEM_Tight");

            //---- SF
            systematicTree->makeOutputVariable(m_ph_SF_ID,      "ph_SF_ID");
            systematicTree->makeOutputVariable(m_ph_SF_ID_unc,  "ph_SF_ID_unc");
            systematicTree->makeOutputVariable(m_ph_SF_iso,     "ph_SF_iso");
            systematicTree->makeOutputVariable(m_ph_SF_iso_unc, "ph_SF_iso_unc");

            //---- truth information
            systematicTree->makeOutputVariable(m_ph_truthType,   "ph_truthType");
            systematicTree->makeOutputVariable(m_ph_truthOrigin, "ph_truthOrigin");
            systematicTree->makeOutputVariable(m_ph_mc_pid,      "ph_mc_pid");
            systematicTree->makeOutputVariable(m_ph_mc_pt,       "ph_mc_pt");
            systematicTree->makeOutputVariable(m_ph_mc_eta,      "ph_mc_eta");
            systematicTree->makeOutputVariable(m_ph_mc_phi,      "ph_mc_phi");
            systematicTree->makeOutputVariable(m_ph_mcel_dr,     "ph_mcel_dr");
            systematicTree->makeOutputVariable(m_ph_mcel_pt,     "ph_mcel_pt");
            systematicTree->makeOutputVariable(m_ph_mcel_eta,    "ph_mcel_eta");
            systematicTree->makeOutputVariable(m_ph_mcel_phi,    "ph_mcel_phi");

            systematicTree->makeOutputVariable(m_ph_mc_parent_pid,    "ph_mc_parent_pid");
            systematicTree->makeOutputVariable(m_ph_mc_production,    "ph_mc_production");

            systematicTree->makeOutputVariable(m_el_isoGradient, "el_isoGradient");
            systematicTree->makeOutputVariable(m_el_mc_pid,      "el_mc_pid");
            systematicTree->makeOutputVariable(m_el_mc_charge,   "el_mc_charge");
            systematicTree->makeOutputVariable(m_el_mc_pt,       "el_mc_pt");
            systematicTree->makeOutputVariable(m_el_mc_eta,      "el_mc_eta");
            systematicTree->makeOutputVariable(m_el_mc_phi,      "el_mc_phi");

            systematicTree->makeOutputVariable(m_event_in_overlap, "event_in_overlap");

        //    systematicTree->makeOutputVariable(m_weight_mm, "weight_MM");
        //    systematicTree->makeOutputVariable(m_weight_mm_TF, "weight_MM_TF");
        }


    }


    //---------- saveEvent - run for every systematic and every event
    void TtGammaEventSaver::saveEvent(const top::Event& event)
    {

        //----------- resize all vectors
        unsigned int i(0);
        unsigned int n_photons = event.m_photons.size();
        unsigned int n_electrons = event.m_electrons.size();

        m_ph_topoetcone20.resize(n_photons);
        m_ph_topoetcone30.resize(n_photons);
        m_ph_topoetcone40.resize(n_photons);

        m_ph_ptcone20.resize(n_photons);
        m_ph_ptcone30.resize(n_photons);
        m_ph_ptcone40.resize(n_photons);


        /*m_ph_ptcone20_TightTTVA_pt1000.resize(n_photons);

        m_ph_topoetcone20_IsoCorrTool.resize(n_photons);
        m_ph_topoetcone40_IsoCorrTool.resize(n_photons);*/


        m_ph_isTight.resize(n_photons);
        m_ph_isTight_daod.resize(n_photons);
        m_ph_isLoose.resize(n_photons);
        m_ph_isHadronFake.resize(n_photons);

        m_ph_truthType.resize(n_photons);
        m_ph_truthOrigin.resize(n_photons);
        m_ph_mc_pid.resize(n_photons);
        m_ph_mc_pt.resize(n_photons);
        m_ph_mc_eta.resize(n_photons);
        m_ph_mc_phi.resize(n_photons);
        m_ph_mcel_pt.resize(n_photons);
        m_ph_mcel_eta.resize(n_photons);
        m_ph_mcel_phi.resize(n_photons);
        m_ph_mcel_dr.resize(n_photons);
        m_ph_OQ.resize(n_photons);
        m_ph_author.resize(n_photons);
        m_ph_conversionType.resize(n_photons);
        m_ph_caloEta.resize(n_photons);
        m_ph_isoFCTCO.resize(n_photons);
        m_ph_isoFCT.resize(n_photons);
        m_ph_isoFCL.resize(n_photons);
        m_ph_isEM_Tight.resize(n_photons);
        m_ph_SF_ID.resize(n_photons);
        m_ph_SF_ID_unc.resize(n_photons);
        m_ph_SF_iso.resize(n_photons);
        m_ph_SF_iso_unc.resize(n_photons);

        m_ph_rhad1.resize(n_photons);
        m_ph_rhad.resize(n_photons);
        m_ph_reta.resize(n_photons);
        m_ph_weta2.resize(n_photons);
        m_ph_rphi.resize(n_photons);
        m_ph_ws3.resize(n_photons);
        m_ph_wstot.resize(n_photons);
        m_ph_fside.resize(n_photons);
        m_ph_deltaE.resize(n_photons);
        m_ph_eratio.resize(n_photons);
        m_ph_emaxs1.resize(n_photons);
        m_ph_f1.resize(n_photons);
        m_ph_e277.resize(n_photons);

        m_el_isoGradient.resize(n_electrons);
        m_el_mc_pid.resize(n_electrons);
        m_el_mc_charge.resize(n_electrons);
        m_el_mc_pt.resize(n_electrons);
        m_el_mc_eta.resize(n_electrons);
        m_el_mc_phi.resize(n_electrons);

        m_ph_mc_parent_pid.resize(n_photons);
        m_ph_mc_production.resize(n_photons);


        //----------- list of relevant truth electrons
        std::vector<const xAOD::TruthParticle*> list_mc_electrons;

        if (m_configMine->isMC()) {

            for (const xAOD::TruthParticle*  part : *event.m_truth) {

                if ((part->pt() > 10000) && (abs(part->pdgId()) == 11) && (fabs(part->eta()) < 3)) {
                    list_mc_electrons.push_back(part);
                }
            }
        }


        //----------- loop over electrons
        i = 0;

        for (const auto* const elPtr : event.m_electrons) {

            m_el_isoGradient[i] = (elPtr->auxdataConst<char>("AnalysisTop_Isol_Gradient")) ? true : false;

            const xAOD::TruthParticle* eltrue = xAOD::TruthHelpers::getTruthParticle(*elPtr);
            if (eltrue != nullptr) {
                m_el_mc_pid[i] = eltrue->pdgId();
                m_el_mc_charge[i] = eltrue->charge();
                m_el_mc_pt[i] = eltrue->pt();
                m_el_mc_eta[i] = eltrue->eta();
                m_el_mc_phi[i] = eltrue->phi();
            }

            ++i;

        }


        //----------- loop over photons

        i = 0;



        //std::cout << "###########################" << std::endl;

        for (const auto* const phPtr : event.m_photons) {

            phPtr->auxdecor< int >( "i" ) = i;

            m_ph_isLoose[i]      = m_photonLooseIsEMSelector->accept(phPtr);
            m_ph_isTight[i]      = m_photonTightIsEMSelector->accept(phPtr);
            m_ph_isEM_Tight[i]   = m_photonTightIsEMSelector->IsemValue();
            m_ph_isTight_daod[i] = (phPtr->auxdataConst<char>("DFCommonPhotonsIsEMTight")) ? true : false;
            m_ph_isHadronFake[i] = isHadronFakePhoton(phPtr);

            phPtr->showerShapeValue(m_ph_rhad1[i],  xAOD::EgammaParameters::Rhad1);
            phPtr->showerShapeValue(m_ph_rhad[i],   xAOD::EgammaParameters::Rhad);
            phPtr->showerShapeValue(m_ph_reta[i],   xAOD::EgammaParameters::Reta);
            phPtr->showerShapeValue(m_ph_weta2[i],  xAOD::EgammaParameters::weta2);
            phPtr->showerShapeValue(m_ph_rphi[i],   xAOD::EgammaParameters::Rphi);
            phPtr->showerShapeValue(m_ph_ws3[i],    xAOD::EgammaParameters::weta1);
            phPtr->showerShapeValue(m_ph_wstot[i],  xAOD::EgammaParameters::wtots1);
            phPtr->showerShapeValue(m_ph_fside[i],  xAOD::EgammaParameters::fracs1);
            phPtr->showerShapeValue(m_ph_deltaE[i], xAOD::EgammaParameters::DeltaE);
            phPtr->showerShapeValue(m_ph_eratio[i], xAOD::EgammaParameters::Eratio);
            phPtr->showerShapeValue(m_ph_emaxs1[i], xAOD::EgammaParameters::emaxs1);
            phPtr->showerShapeValue(m_ph_f1[i],     xAOD::EgammaParameters::f1);
            phPtr->showerShapeValue(m_ph_e277[i],   xAOD::EgammaParameters::e277);

            phPtr->isolationValue( m_ph_topoetcone20[i] , xAOD::Iso::topoetcone20 );
            phPtr->isolationValue( m_ph_topoetcone30[i] , xAOD::Iso::topoetcone30 );
            phPtr->isolationValue( m_ph_topoetcone40[i] , xAOD::Iso::topoetcone40 );

            //------ Saving new track isolation variables
      //      phPtr->isolationValue(m_ph_ptcone20_TightTTVA_pt500[i], xAOD::Iso::ptcone20_TightTTVA_pt500);
      //      phPtr->isolationValue(m_ph_ptcone20_TightTTVA_pt1000[i], xAOD::Iso::ptcone20_TightTTVA_pt1000);
      //      phPtr->isolationValue(m_ph_ptvarcone30_TightTTVA_pt500[i], xAOD::Iso::ptvarcone30_TightTTVA_pt500);
      //      phPtr->isolationValue(m_ph_ptvarcone30_TightTTVA_pt1000[i], xAOD::Iso::ptvarcone30_TightTTVA_pt1000);


            phPtr->isolationValue( m_ph_ptcone20[i] , xAOD::Iso::ptcone20 );
            phPtr->isolationValue( m_ph_ptcone30[i] , xAOD::Iso::ptcone30 );
            phPtr->isolationValue( m_ph_ptcone40[i] , xAOD::Iso::ptcone40 );

            m_ph_isoFCT[i]   = (phPtr->auxdataConst<char>("AnalysisTop_Isol_FixedCutTight")) ? true : false;
            m_ph_isoFCTCO[i] = (phPtr->auxdataConst<char>("AnalysisTop_Isol_FixedCutTightCaloOnly")) ? true : false;
            m_ph_isoFCL[i]   = (phPtr->auxdataConst<char>("AnalysisTop_Isol_FixedCutLoose")) ? true : false;


            //----------- truth information

            m_ph_truthType[i]   = -1;
            m_ph_truthOrigin[i] = -1;

            m_ph_mc_pid[i] = -1;
            m_ph_mc_pt[i]  = -1;
            m_ph_mc_eta[i] = -1;
            m_ph_mc_phi[i] = -1;

            m_ph_mcel_pt[i]  = -1;
            m_ph_mcel_eta[i] = -1;
            m_ph_mcel_phi[i] = -1;
            m_ph_mcel_dr[i]  = -1;

            //---------- Applying correction to calorimeter isolation

        /*    xAOD::PhotonContainer photons = event.m_photons;
            for(int ph(0); ph < n_photons; ph++){

              top::check(m_isoCorrTool->applyCorrection(*photons[ph]), "Failed to apply correction");

              photons[ph]->isolationValue(m_ph_topoetcone20_IsoCorrTool[ph] , xAOD::Iso::topoetcone20 );
              photons[ph]->isolationValue(m_ph_topoetcone40_IsoCorrTool[ph] , xAOD::Iso::topoetcone40 );


            }*/

            if (m_configMine->isMC()){

                static SG::AuxElement::Accessor<int> typeph("truthType");
                static SG::AuxElement::Accessor<int> originph("truthOrigin");

                m_ph_truthType[i]   = typeph(*phPtr);
                m_ph_truthOrigin[i] = originph(*phPtr);

                const xAOD::TruthParticle* phtrue = xAOD::TruthHelpers::getTruthParticle(*phPtr);
                if (phtrue != nullptr) {
                    m_ph_mc_pid[i] = phtrue->pdgId();
                    m_ph_mc_pt[i]  = phtrue->pt();
                    m_ph_mc_eta[i] = phtrue->eta();
                    m_ph_mc_phi[i] = phtrue->phi();

						bool prodVtx = true;
						const xAOD::TruthParticle* curpart = phtrue;

			//		std::cout << "------------------------" << std::endl;
						if(curpart->pdgId() != 22) {
		   //       std::cout << "------------------------" << std::endl;
			  //      std::cout << "Current Particle: " << curpart->pdgId() << std::endl;
				//			std::cout << "Current Particle hadron: " << curpart->isHadron() << std::endl;
				//			std::cout << "Current Particle status: " << curpart->status() << std::endl;
              prodVtx = false;
              m_ph_mc_production[i] = 0;
              m_ph_mc_parent_pid[i] = curpart->parent()->pdgId();
            }

						while(prodVtx){

							if(curpart->pdgId() != 22 && curpart->status() > 20 && curpart->status() <30){

                if(curpart->status() == 21) m_ph_mc_production[i] = 1;
                else if(curpart->status() == 22 || curpart->status() == 23) m_ph_mc_production[i] = 2;
                else m_ph_mc_production[i] = 0;
                m_ph_mc_parent_pid[i] = curpart->pdgId();

								break;
							}

              curpart = curpart->parent();

              if(curpart == nullptr){
			    //std::cout << "no parent" << std::endl;
                m_ph_mc_production[i] = 0;
                m_ph_mc_parent_pid[i] = 0;
                break;

              }


              if(curpart->isHadron()){
			//	 std::cout << "Current Particle: " << curpart->pdgId() << std::endl;
			//				std::cout << "Current Particle hadron: " << curpart->isHadron() << std::endl;
			//				std::cout << "Current Particle status: " << curpart->status() << std::endl;
                m_ph_mc_production[i] = 0;
                m_ph_mc_parent_pid[i] = curpart->pdgId();

                break;
              }



            }

                for (const auto* const part : list_mc_electrons){

                    float dr = phPtr->p4().DeltaR(part->p4());

                    if ((m_ph_mcel_dr[i] == -1) || (m_ph_mcel_dr[i] > dr)){
                        m_ph_mcel_dr[i] = dr;
                        m_ph_mcel_pt[i] = part->pt();
                        m_ph_mcel_eta[i] = part->eta();
                        m_ph_mcel_phi[i] = part->phi();
                    }
                }
            }

          }
            //----------- reconstruction

            m_ph_OQ[i]      = phPtr->OQ();
            m_ph_author[i]  = phPtr->author();

            m_ph_conversionType[i] = phPtr->conversionType();
            m_ph_caloEta[i]        = (phPtr->caloCluster()) ? phPtr->caloCluster()->etaBE(2) : -5;


            //----------- scale factors for identification and isolation

            m_ph_SF_ID[i] = 1;
            m_ph_SF_ID_unc[i] = 0;

            double SF_iso = 1.;
            double unc_SF_iso = 0.;

            if(m_configMine->isMC()){

                if (phPtr->isAvailable<float>("EFF_ID_SF"))    m_ph_SF_ID[i]     = phPtr->auxdataConst<float>("EFF_ID_SF");
                if (phPtr->isAvailable<float>("EFF_ID_SF_UP")) m_ph_SF_ID_unc[i] = phPtr->auxdataConst<float>("EFF_ID_SF_UP");

                m_ph_SF_ID_unc[i] -= m_ph_SF_ID[i];

                top::check(m_photonEfficiencyCorrectionTool->getEfficiencyScaleFactor(*phPtr, SF_iso), "photonEfficiencyCorrectionTool failed 11");
                top::check(m_photonEfficiencyCorrectionTool->getEfficiencyScaleFactorError(*phPtr, unc_SF_iso), "photonEfficiencyCorrectionTool failed 12");
            }

            m_ph_SF_iso[i]     = SF_iso;
            m_ph_SF_iso_unc[i] = unc_SF_iso;

            ++i;
        }

        // Overlap removal for X+jets and X+jets+gamma events

        bool in_overlap;
		if (topConfig()->isMC()) {
		top::check(m_vgamma_tool->inOverlap(in_overlap), "Could not execute VGammaORTool::inOverlap(bool)");
		}
		m_event_in_overlap = static_cast<char>(in_overlap);

    top::EventSaverFlatNtuple::saveEvent(event);
}



    bool TtGammaEventSaver::isHadronFakePhoton (const xAOD::Photon* phPtr) {


        //----------- (1) check if photon passes requirements on author, object quality, cleaning and pt and eta cuts

        if (phPtr->author() != xAOD::EgammaParameters::AuthorPhoton && phPtr->author() != xAOD::EgammaParameters::AuthorAmbiguous)
            return false;

        if (!phPtr->isGoodOQ(xAOD::EgammaParameters::BADCLUSPHOTON))
            return false;

        if ((phPtr->OQ() & 134217728) != 0 && (phPtr->showerShapeValue(xAOD::EgammaParameters::Reta) > 0.98 || phPtr->showerShapeValue(xAOD::EgammaParameters::Rphi) > 1.0 || (phPtr->OQ() & 67108864) != 0))
            return false;


        //----------- (2) check if photon fails tight ID cuts

        bool tight    = m_photonTightIsEMSelector->accept(phPtr);
        int isEMValue = m_photonTightIsEMSelector->IsemValue();

        if (tight)
            return false;


        //----------- (3) check if photon passes relaxed tight cuts. Consequently, at least one cut on the strip variables is failed: ws2, fside, deltaE and Eratio.

        const unsigned int tightBits[7] = {
            egammaPID::ClusterHadronicLeakage_Photon, //Rhad or Rhad1
            egammaPID::ClusterMiddleEnergy_Photon,    //E2,7×7 (very mild cut to reject photons with poorly measured energies)
            egammaPID::ClusterMiddleEratio37_Photon,  //Reta
            egammaPID::ClusterMiddleEratio33_Photon,  //Rphi
            egammaPID::ClusterMiddleWidth_Photon,     //weta2
            egammaPID::ClusterStripsEratio_Photon,    //f1 (very mild cut to reject photons with poorly measured energies)
            egammaPID::ClusterStripsWtot_Photon       //wstot
        };

        for (int j = 0; j < 7; j++) {
            if ( (isEMValue)&(1<<(tightBits[j])) ) return false; // the bit is equal to 1, the corresponding cut is failed
        }

        return true;
    }
}
