LibraryNames libTopEventSelectionTools libTopEventReconstructionTools libTtGamma

### Good Run List
GRLDir GoodRunsLists
GRLFile data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml
PRWConfigFiles_FS dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.FS.v2/prw.merged.root
PRWConfigFiles_AF dev/AnalysisTop/PileupReweighting/user.iconnell.Top.PRW.MC16d.AF.v2/prw.merged.root
PRWActualMu_FS GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRWActualMu_AF GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRWLumiCalcFiles GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root

# luminosities from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopDerivationMC16aList
# luminosity 2017: 44307.4 pb-1

ElectronCollectionName Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMPFlowJets_BTagging201903
LargeJetCollectionName None
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName Photons

TruthCollectionName TruthParticles
TruthJetCollectionName AntiKt4TruthWZJets
TopPartonHistory False
TopParticleLevel False
TruthBlockInfo False
PDFInfo True

MCGeneratorWeights Nominal
#LHAPDFBaseSet NNPDF30_nnlo_as_0118
#LHAPDFSets PDF4LHC15_nnlo_30_pdfas
#LHAPDFEventWeights Nominal

ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::TtGammaEventSaver
OutputEvents SelectedEvents
OutputFilename output.root
PerfStats No

Systematics All
JetUncertainties_NPModel CategoryReduction
JetUncertainties_BunchSpacing 25ns

ElectronPt 25000.
ElectronID TightLH
ElectronIsolation FCTight
ElectronIDLoose MediumLH
ElectronIsolationLoose None

MuonPt 25000.
MuonQuality Medium
MuonIsolation FCTight
MuonQualityLoose Medium
MuonIsolationLoose None

PhotonEta 2.37         #default is 2.5
PhotonPt 15000.        #default is 25000.
PhotonID Tight          #default is Tight
PhotonIsolation FixedCutTight   #default is FixedCutTight

JetPt 25000.
JetEta 4.50
LargeRJetEta 4.50
TruthJetEta 4.50
TruthPhotonPt 10000.

# PhotonIDLoose None        #default is Loose
# PhotonIsolationLoose None #default is FixedCutLoose
# PhotonUseRadiativeZ False

# DoTight/DoLoose to activate the loose and tight trees
# each should be one in: Data, MC, Both, False
DoTight Both
DoLoose Data

## Matrix Method Weights calculation. [Must set DoLoose flag to Both or Data]
FakesMMWeightsIFF True
FakesMMIFFDebug   True
FakesMMConfigIFF  dev/AnalysisTop/FakeBkgToolsData/Efficiency2D_Data.xml:1T:0R[L]

UseAodMetaData True
#IsAFII False

BTaggingWP DL1r:FixedCutBEff_60 DL1r:FixedCutBEff_70 DL1r:FixedCutBEff_77 DL1r:FixedCutBEff_85 DL1r:Continuous
BTagCDIPath xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2020-03-11_v3.root

FakesControlRegionDoLooseMC False
OverlapRemovalLeptonDef Tight
OverlapRemovalProcedure recommended
ApplyElectronInJetSubtraction False


########################
### Global definition of the triggers
########################

UseGlobalLeptonTriggerSF True
ElectronTriggers      2015@e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose 2016@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 2017@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
ElectronTriggersLoose 2015@e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose 2016@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0 2017@e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
MuonTriggers          2015@mu20_iloose_L1MU15_OR_mu50 2016@mu26_ivarmedium_OR_mu50 2017@mu26_ivarmedium_OR_mu50
MuonTriggersLoose     2015@mu20_iloose_L1MU15_OR_mu50 2016@mu26_ivarmedium_OR_mu50 2017@mu26_ivarmedium_OR_mu50


########################
### basic selection with mandatory cuts for reco level
########################

SUB BASIC
INITIAL
GRL
GOODCALO
PRIVTX
RECO_LEVEL
NOBADMUON

########################
### lepton trigger and offline cuts for reco-level selections
########################

SUB EL_TRIGGER
. BASIC
GTRIGDEC
EL_N 25000 >= 1


SUB MU_TRIGGER
. BASIC
GTRIGDEC
MU_N 25000 >= 1


########################
### triggermatch
########################

SELECTION triggermatch_el_no_jet
. EL_TRIGGER
MU_N 25000 == 0
GTRIGMATCH
JET_N 25000 == 0
JETCLEAN LooseBad
MET < 32000
SAVE

SELECTION triggermatch_el_gamma
. EL_TRIGGER
GTRIGMATCH
MU_N 25000 == 0
PH_N 15000 >= 1
SAVE

SELECTION triggermatch_mu_gamma
. MU_TRIGGER
GTRIGMATCH
EL_N 25000 == 0
PH_N 15000 >= 1
SAVE


########################
### e+jets selections
########################

SELECTION sel_ejets
. EL_TRIGGER
EL_N 25000 == 1
MU_N 25000 == 0
GTRIGMATCH
JETCLEAN LooseBad
JET_N 25000 >= 1
PH_N 15000 >= 1
MET > 25000
SAVE


########################
### mu+jets selections
########################

SELECTION sel_mujets
. MU_TRIGGER
EL_N 25000 == 0
MU_N 25000 == 1
GTRIGMATCH
JETCLEAN LooseBad
JET_N 25000 >= 1
PH_N 15000 >= 1
MET > 25000
SAVE


########################
### Z-->ee
########################

SELECTION sel_zee_OS
. EL_TRIGGER
EL_N 25000 == 2
MU_N 25000 == 0
OS
#MLL > 57000
JET_N 25000 == 0
JETCLEAN LooseBad
MET < 32000
PH_N 15000 == 0
SAVE


########################
### Z-->eg
########################

SELECTION sel_zeg
. EL_TRIGGER
EL_N 25000 == 1
MU_N 25000 == 0
GTRIGMATCH
JET_N 25000 == 0
JETCLEAN LooseBad
MET < 32000
PH_N 15000 >= 1
SAVE


########################
### Z-->eeg
########################

SELECTION sel_radZ_eeg_OS
. EL_TRIGGER
EL_N 25000 == 2
MU_N 25000 == 0
OS
PH_N 15000 >= 1
SAVE

########################
### Z-->mumug
########################

SELECTION sel_radZ_mumug_OS
. MU_TRIGGER
EL_N 25000 == 0
MU_N 25000 == 2
OS
PH_N 15000 >= 1
SAVE
