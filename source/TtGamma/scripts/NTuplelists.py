import ROOT
import os, sys
from array import array
import pandas as pd

    
df = pd.read_csv('sample_info.csv')

indirs = ["/eos/user/w/wendlanb/SgTopGamma/NTuple_v1/","/eos/user/w/wendlanb/SgTopGamma/NTuple_v2/"]

campaign = ["mc16a/", "mc16d/","mc16e/"]

os.system("rm -R NTuplelists")

indir = ""
for camp in campaign:
    if camp == "mc16e/": indir = indirs[1]
    else: indir = indirs[0]
    for cur_dir in os.listdir(indir+camp):
        
        
         
        if ".root" not in cur_dir: continue
              
        cur_dir_strings = cur_dir.split(".")
        
        dsid = cur_dir_strings[2]
        
        MC_type = df.loc[(df['DSID']==int(dsid)) & (df['campaign'] == camp.replace("/","").strip())]['type'].values
        os.system("mkdir -p NTuplelists")
        
        os.system("touch NTuplelists/"+MC_type[0]+"_"+camp.replace("/","").strip()+".txt")
        outfile = open("NTuplelists/"+MC_type[0]+"_"+camp.replace("/","").strip()+".txt", "a")
        
        for cur_file in os.listdir(indir+camp+cur_dir):
         
             process = True
             if ".root" in cur_file:
                  for zombie in open("zombie_files.txt"):
                      if cur_file.strip() in zombie.strip(): 
                          process =False
                  if process: outfile.write(camp+cur_dir+"/"+cur_file+"\n")  
                 
       
datacamp = ["data15/", "data16/", "data17/", "data18/"]
indir = ""
for data in datacamp:       
	if data == "data18/": indir = indirs[1]
	else: indir = indirs[0]
	for cur_dir in os.listdir(indir+data):
		if ".root" not in cur_dir: continue
              
        cur_dir_strings = cur_dir.split(".")
        
        os.system("touch NTuplelists/"+data.replace("/","").strip()+".txt")
        outfile = open("NTuplelists/"+data.replace("/","").strip()+".txt", "a")
        
        for cur_file in os.listdir(indir+data+cur_dir):
			if ".root" in cur_file:
				outfile.write(data+cur_dir+"/"+cur_file+"\n")
        
        
