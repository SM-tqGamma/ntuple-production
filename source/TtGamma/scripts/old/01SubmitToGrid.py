'''

 WARNING  Check if the flag 'NEvents' is removed from the cut file!

 Useful flags:

     config.maxNFilesPerJob = <int>
     config.otherOptions = '<option for TopExamples.grid>'
         '--forceStaged': other treatment of I/O at TopExamples.grid)
         --extFile prw.config.MC16d.root': uploading additional file for job

'''


#!/usr/bin/env python

import TopExamples.grid
import Data_rel21
import MC16a_TOPQ1
import MC16d_TOPQ1
import MC16e_TOPQ1
import os,sys
from datetime import datetime

now = datetimen.now()
def main(periods):

    config = TopExamples.grid.Config()
    config.code          = 'top-xaod'
    config.gridUsername  = os.getenv("RUCIO_ACCOUNT", "hpotti")
    config.suffix        = "v1"+".%s_%s_%s" %(now.strftime('%d'), now.strftime("%b"), now.strftime("%y"))
    config.excludedSites = ''
    config.noSubmit      = True
    config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
    config.destSE        = 'SMU2_LOCALGROUPDISK' #This is the default (anywhere), or try e.g. 'UKI-SOUTHTopExamples.grid-BHAM-HEP_LOCALGROUPDISK'\

    #config.maxNFilesPerJob = '5'

    # by default the requested memory is set to 2GB, if you need to increase this, please disable the line below!!!
    #config.memory = '14000'


    ###############################################################################

    # Data - look in Data.py
    if "data15_16" in periods:

        config.settingsFile = 'cuts_rel21_MC16a_top_FS.txt'
        names = ['TOPQ1_data_allYear_2015_2016']

        samples = TopExamples.grid.Samples(names)
        TopExamples.grid.submit(config, samples)

    if "data17" in periods:
        config.settingsFile = 'cuts_rel21_MC16d_top_FS.txt'
        names = ['TOPQ1_data_allYear_2017']

        samples = TopExamples.grid.Samples(names)
        TopExamples.grid.submit(config, samples)
    if "data18" in periods:
        config.settingsFile = 'cuts_rel21_MC16e_top_FS.txt'
        names = ['TOPQ1_data_allYear_2018']

        samples = TopExamples.grid.Samples(names)
        TopExamples.grid.submit(config, samples)

    ###############################################################################

    ### MC Simulation - look in MC16a_TOPQ1.py and MC16d_TOPQ1.py
    ### Using list of TOPQ1 25ns MC samples, consistent mixture of p-tags

    ### MC16a


    ### ttbar, singleTop

    if "mc16a" in periods:

        config.settingsFile = 'cuts_rel21_MC16a_top_FS.txt'
        names = ['MC16a_TOPQ1_ttbar_PowPy8', 'MC16a_TOPQ1_singleTop']

        samples = TopExamples.grid.Samples(names)
        TopExamples.grid.submit(config, samples)

    if "mc16d" in periods:

        ### ttbar, singleTop

        config.settingsFile = 'cuts_rel21_MC16d_top_FS.txt'
        names = ['MC16d_TOPQ1_ttbar_PowPy8', 'MC16d_TOPQ1_singleTop']

        samples = TopExamples.grid.Samples(names)
        TopExamples.grid.submit(config, samples)


    if "mc16e" in periods:

        ### ttbar, singleTop

        config.settingsFile = 'cuts_rel21_MC16e_top_FS.txt'
        names = ['MC16e_TOPQ1_ttbar_PowPy8', 'MC16e_TOPQ1_singletop']

        samples = TopExamples.grid.Samples(names)
        TopExamples.grid.submit(config, samples)


if __name__ == "__main__":
    periods = sys.argv[1].split(",")
    
    main(periods)
