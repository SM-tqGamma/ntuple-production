This folder contains the grid submission scripts and the sample lists. 

After downloading finished samples you should check if the number of events you find in the first cutflow bin agrees between the locally saved samples and the ones you can find in Rucio. 
Therefore run the "check_number_of_events.py" script which will give you the numbers of events for both samples and their difference. 
A "0" in the last column tells you that everything has been processed for this sample. It will create a ".csv" file which contains information about the NTuple status and cross sections of the samples.

The script "sample_table" will create a table that tells you the NTuple status in mc16a,d,e for all MC samples. 


TODO here:

1) Check for the non 0 entries in mc16d
2) 410470 (ttbar) is not finished in mc16a,d
3) MC16e sample list copied from ttgamma (other Z+y+jets samples)
4) W+y+jets not available in mc16e?
5) Other postprocessing scripts should be implemented
6) Implement vgamma_OR tool
7) Write fJVT out?
