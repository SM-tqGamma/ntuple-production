# infos taken on 2018-11-22 from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopDerivationMC16aList

import TopExamples.grid

# containers for data for years 2015, 2016, 2017 in release 21

TopExamples.grid.Add("data1516").datasets = [
    #'data15_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp15_v01_p4030',
    #'data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp16_v01_p4030',
    "data15_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp15_v01_p4173",
    "data16_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp16_v01_p4173",
]

TopExamples.grid.Add("data17").datasets = [  
    #'data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp17_v01_p4030',
    "data17_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp17_v01_p4173",
]


TopExamples.grid.Add("data18").datasets = [  
    #'data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp18_v01_p4030',
    "data18_13TeV.AllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp18_v01_p4173",
]

