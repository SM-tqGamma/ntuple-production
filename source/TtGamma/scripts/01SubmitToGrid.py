'''

 WARNING  Check if the flag 'NEvents' is removed from the cut file!

 Useful flags:

     config.maxNFilesPerJob = <int>
     config.otherOptions = '<option for TopExamples.grid>'
         '--forceStaged': other treatment of I/O at TopExamples.grid)
         --extFile prw.config.MC16d.root': uploading additional file for job

'''


#!/usr/bin/env python

import TopExamples.grid
import Data_rel21
import MC16a_TOPQ1
import MC16d_TOPQ1
import MC16e_TOPQ1
import os,sys
from datetime import datetime

now = datetime.now()
def main(periods):

    config = TopExamples.grid.Config()
    config.code          = 'top-xaod'
    config.gridUsername  = os.getenv("RUCIO_ACCOUNT", "phys-top")
    config.suffix        = "_tqgamma"+".%s_%s_%s" %(now.strftime('%d'), now.strftime("%b"), now.strftime("%y"))
    config.excludedSites = ''
    config.noSubmit      = False
    config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
    config.destSE        = 'BNL-OSG2_LOCALGROUPDISK' #This is the default (anywhere), or try e.g. 'UKI-SOUTHTopExamples.grid-BHAM-HEP_LOCALGROUPDISK'\

    #config.maxNFilesPerJob = '5'

    # by default the requested memory is set to 2GB, if you need to increase this, please disable the line below!!!
    #config.memory = '14000'



    ### MC Simulation - look in MC16a_TOPQ1.py and MC16d_TOPQ1.py
    ### Using list of TOPQ1 25ns MC samples, consistent mixture of p-tags


    if "mc16a" in periods:

        config.settingsFile = 'config_mc16a.txt'
        names = ['MC16a_crucial', 'MC16a_additional', "data1516"]

        samples = TopExamples.grid.Samples(names)
        TopExamples.grid.submit(config, samples)

    if "mc16d" in periods:

        config.settingsFile = 'config_mc16d.txt'
        names = ['MC16d_crucial', 'MC16d_additional', "data17"]

        samples = TopExamples.grid.Samples(names)
        TopExamples.grid.submit(config, samples)


    if "mc16e" in periods:

        config.settingsFile = 'config_mc16e.txt'
        names = ['MC16e_crucial', 'MC16e_additional', "data18"]

        samples = TopExamples.grid.Samples(names)
        TopExamples.grid.submit(config, samples)


if __name__ == "__main__":
    periods = sys.argv[1].split(",")
    
    main(periods)
