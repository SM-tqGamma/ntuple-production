import os, sys
from array import array
import pandas as pd
import numpy as np

finished_dsid = []
df = pd.read_csv("sample_info.csv")
df = df.drop(columns=['Unnamed: 0'])
df['type'] = np.zeros(len(df['DSID']))

ttgamma = [410389]
ttbar = [410470]
Zjets = list(range(364100,364142))
Wjets = list(range(364156,364198))
Zjetsgamma = list(range(364500,364515))+[364517,364518,364519]+list(range(366140,366155))
Wjetsgamma = list(range(364521,364536))
sgtop = [410658, 410659, 410644, 410645,410646,410647]
diboson = [364250,364253,364254,364255,363355,363356,363357,363358,363359,363360,363489]
topvariations = [411032,411033, 412004, 411034, 411035, 412005, 410654, 410655, 411036, 411037, 412002, 410557, 410464, 410480]
tqgamma = [412089]

names = ['ttGamma', 'ttbar', 'Z+Jets', 'W+Jets', 'Z+Jets+Gamma', 'W+Jets+Gamma', 'Single Top', 'Diboson', 'Top variations', 'tqGamma']

all_types = [ttgamma, ttbar, Zjets, Wjets, Zjetsgamma, Wjetsgamma, sgtop, diboson, topvariations, tqgamma]

outfile = open("../sample_availability.md", "w")

outfile.write("In this table the availabilities of the different samples in mc16a,d,e are listed.\n")
outfile.write("-: sample is fully available \n")
outfile.write("o: sample is available but events are missing \n")
outfile.write("x: sample is not available\n")
outfile.write("\n")
outfile.write("Please note that W+Jets+Gamma samples are not available in mc16e and for Z+Jets+Gamma there are new samples (366...), which will be processed for mc16a,d.\n")
outfile.write("\n\n\n\n")

outfile.write("| **DSID** | **mc16a** | **mc16d** | **mc16e** |\n")
outfile.write("| ---------- | ---------- | ---------- | ---------- |\n")

for type_i in range(len(all_types)):
    outfile.write("| **"+names[type_i]+"** |\n")
    for dsid in all_types[type_i]:
        if dsid in finished_dsid: continue
        camps = (df.loc[df['DSID']==dsid])['campaign'].values
        indices = df.index[df['DSID']==dsid].tolist()
        
        for ind in indices: df.loc[ind, 'type'] = names[type_i].strip().replace(" ","").replace("+","")
        outfile.write("|"+str(dsid)+"|")
        if 'mc16a' in camps: 
            diff_mc16a = int((df.loc[(df['DSID']==dsid) & (df['campaign'] == 'mc16a')])['diff'].values)
        
            if diff_mc16a == 0: outfile.write(' - |')
            else: outfile.write(' o |')
        else: outfile.write(' x |')

        if 'mc16d' in camps: 
            diff_mc16d = int((df.loc[(df['DSID']==dsid) & (df['campaign'] == 'mc16d')])['diff'].values)
            if diff_mc16d == 0: outfile.write(' - |')
            else: outfile.write(' o |')
        else: outfile.write(' x |') 

        if 'mc16e' in camps: 
            diff_mc16e = int((df.loc[(df['DSID']==dsid) & (df['campaign'] == 'mc16e')])['diff'].values)
            if diff_mc16e == 0: outfile.write(' - |\n')
            else: outfile.write(' o |\n')
        else: outfile.write(' x |\n')          
    
        finished_dsid.append(dsid)

df.to_csv('sample_info.csv')
outfile.write("| **others** |\n")
for dsid in df['DSID']:
    
    if dsid in finished_dsid: continue
    camps = (df.loc[df['DSID']==dsid])['campaign'].values
    
    outfile.write("|"+str(dsid)+"|")
    if 'mc16a' in camps: 
       diff_mc16a = int((df.loc[(df['DSID']==dsid) & (df['campaign'] == 'mc16a')])['diff'].values)
        
       if diff_mc16a == 0: outfile.write(' - |')
       else: outfile.write(' o |')
    else: outfile.write(' x |')

    if 'mc16d' in camps: 
        diff_mc16d = int((df.loc[(df['DSID']==dsid) & (df['campaign'] == 'mc16d')])['diff'].values)
        if diff_mc16d == 0: outfile.write(' - |')
        else: outfile.write(' o |')
    else: outfile.write(' x |') 

    if 'mc16e' in camps: 
        diff_mc16e = int((df.loc[(df['DSID']==dsid) & (df['campaign'] == 'mc16e')])['diff'].values)
        if diff_mc16e == 0: outfile.write(' - |\n')
        else: outfile.write(' o |\n')
    else: outfile.write(' x |\n')          
    
    finished_dsid.append(dsid)
