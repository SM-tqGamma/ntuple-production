import ROOT
import os, sys
from array import array
import pandas as pd

import ast

def get_crossx(DSID):

    crossxfile = open("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC15-13TeV.data")
    for line in crossxfile:
        if DSID not in line: continue
        line_strings = line.split(" ")
        line_strings_corr = []
        for st in line_strings:
           if st == "": continue
           line_strings_corr.append(st) 
        return float(line_strings_corr[1].strip())*float(line_strings_corr[2].strip())
    return 0
        



sum_events = 0

indirs = ["/eos/user/w/wendlanb/SgTopGamma/NTuple_v1/","/eos/user/w/wendlanb/SgTopGamma/NTuple_v2/"]

campaign = ["mc16a/", "mc16d/","mc16e/"]

sample_dict = {}

sample_dict['name'] = []
sample_dict['DSID'] = []
sample_dict['sum_events_post'] = []
sample_dict['sum_events_pre'] = []
sample_dict['diff'] = []
sample_dict['sum_weights'] = []
sample_dict['campaign'] = []
sample_dict['xSec'] = []
sample_dict['etag'] = []
sample_dict['stag'] = []
sample_dict['rtag'] = []
sample_dict['ptag'] = []

logfile = open("check_number_log.txt", "w")

indir = ""

for camp in campaign:
    print(camp.replace("/",""))
    print("DSID local rucio diff")
    if camp == "mc16e/": indir = indirs[1]
    else: indir = indirs[0]
    for cur_dir in os.listdir(indir+camp):
        os.system("rm log.txt")
        os.system("rm metadata.txt")
        sum_events = 0
        sum_weights = 0
         
        if ".root" not in cur_dir: continue
        
        sample_dict['campaign'].append(camp.replace("/",""))
        sample_dict['name'].append(cur_dir.replace("user.wendlanb.","").split(".AT")[0])
        cur_dir_strings = cur_dir.split(".")
        ami_tag_strings = cur_dir_strings[5].split("_")
        sample_dict['DSID'].append(cur_dir_strings[2])
        sample_dict['ptag'].append(ami_tag_strings[3]) 
        sample_dict['rtag'].append(ami_tag_strings[2])
        sample_dict['etag'].append(ami_tag_strings[0])
        sample_dict['stag'].append(ami_tag_strings[1])
        sample_dict['xSec'].append(get_crossx(cur_dir_strings[2]))
        
       
        logfile.write(camp.replace("/","")+ ": "+cur_dir_strings[2] +" starting to check \n")
        for cur_file in os.listdir(indir+camp+cur_dir):
             process = True
             for zombie in open("zombie_files.txt"):
                 
                 if cur_file.strip() in zombie.strip(): process =False      
             if(".root" in cur_file and process==True):
                 infile = ROOT.TFile(indir+camp+cur_dir+"/"+cur_file)
                 
                 hist_cut = infile.Get("sel_ejets/cutflow")
                 sum_events += hist_cut.GetBinContent(1)
                 hist_cut_mc_pu_zvtx = infile.Get("sel_ejets/cutflow_mc_pu_zvtx")
                 sum_weights += hist_cut_mc_pu_zvtx.GetBinContent(1)
                 infile.Close()
       
        
        os.system("rucio list-dids --short mc16_13TeV.*"+cur_dir_strings[2]+".*DAOD_TOPQ1.*"+ami_tag_strings[2]+"* >> log.txt")
       
        file = open("log.txt")
        sum_rucio = 0
        
        for line in file:
            if "tid" not in line: continue 
            
            if ami_tag_strings[3] not in line: continue
            if ami_tag_strings[1] not in line: continue
            if ami_tag_strings[0] not in line: continue
            os.system("rucio get-metadata "+line.strip()+" >> metadata.txt" )
         
        metadata = open("metadata.txt")
        for meta in metadata:
        
            if "events:" not in meta: continue
            if meta.replace("events:", "").strip() == 'None': continue
            sum_rucio += int(meta.replace("events:", "").strip())
        sample_dict['sum_events_post'].append(sum_events)
        sample_dict['sum_events_pre'].append(sum_rucio)
        sample_dict['sum_weights'].append(sum_weights)
        sample_dict['diff'].append(sum_rucio-sum_events)     
        print(cur_dir_strings[2]+": "+ str(int(sum_events)) + " " + str(sum_rucio)+ " "+str(sum_rucio-int(sum_events)))
         
        logfile.write(camp.replace("/","")+ ": "+cur_dir_strings[2] +" checked \n")       
 
df = pd.DataFrame.from_dict(sample_dict)
df.to_csv('sample_info.csv')    
 

datasets = ['data15', 'data16', 'data17', 'data18']


sample_dict = {}

sample_dict['name'] = []
sample_dict['DSID'] = []
sample_dict['sum_events_post'] = []
sample_dict['sum_events_pre'] = []
sample_dict['diff'] = []

print("DSID local rucio diff")
for dataset in datasets:
    datafile = open("data_samples.txt")
    sum_rucio = 0
    logfile.write(dataset +" starting to check \n")
    for sample in datafile:
        
        if dataset not in sample: continue
        
        sample_dict['name'].append(sample.strip().replace(dataset+"_13TeV:"+dataset+"_13TeV.",""))
        sample_dict['DSID'].append(dataset.strip())
        os.system("rucio list-content --short "+sample.strip()+ " >> datasets.txt")
        datasetfile = open("datasets.txt")
        for ds in datasetfile:
            
            os.system("rucio get-metadata "+ds.strip()+" >> metadata.txt") 
        metadata = open("metadata.txt")
        for meta in metadata:
            if "events:" not in meta: continue
            
            sum_rucio += int(meta.replace("events:", "").strip())
    sample_dict['sum_events_pre'].append(sum_rucio)
         
    os.system("rm datasets.txt metadata.txt")
    
    sum_events = 0
    for cur_dir in os.listdir(indir+dataset+"/"):
        if ".root" not in cur_dir: continue
        
        for cur_file in os.listdir(indir+dataset+"/"+cur_dir):
             
             if ".root" in cur_file:
                
                 infile = ROOT.TFile(indir+dataset+"/"+cur_dir+"/"+cur_file)
         
                 hist_cut = infile.Get("sel_ejets/cutflow")
                 
                 sum_events += hist_cut.GetBinContent(1)
                
    sample_dict['sum_events_post'].append(sum_events)
    sample_dict['diff'].append(sum_events-sum_rucio)
    print(dataset+" "+str(sum_events)+ " "+str(sum_rucio)+" "+str(sum_events-sum_rucio))
    logfile.write(dataset +" checked \n") 
         
df = pd.DataFrame.from_dict(sample_dict)
df.to_csv('datasets_info.csv')     

logfile.close()


